'''
Created on Apr 9, 2019

@author: cam
'''
# Görüldüğü üzere çok zorlu şekilde verilerimizi
# kullanıcının hiç hata yapmadan verileri gireceği 
# varsayımı ile kaydedip okumayı başardık.

# fakat verilerimiz çok büyürse, bunların herbirini
# program yüklenirken okuyup hafızaya yüklemek 
# ve ayrıca büyük veriler arasında for vb döngüler ile
# karmaşık , esnek aramalar yapmak imkansız gibi.

# daha kolayı var. SQL komutları ile kolayca 
# verileri süzüp, güncelleyebileceğimiz
# veri tabanı kullanmak.

# bunun için veritabanı sunucusu kurmak gerekiyor.
# fakat sqlite sayesinde buna da gerek yok.

# ekstra not : ayrıca kastettiğimiz veri tabanı ilişkisel veri tabanları
# nosql denilen, döküman tabanlılar değil...

# Veri ile işleri olan kişiler toplamda (genel olarak) 4 işlem yaparlar
# CRUD kısaltmasını kullanıyor yabancılar
# C : create , oluştur, ekle
# R : read , oku
# U : update , güncelle
# D : delete , sil

# SQL yapısal sorgu dili demektir ve 
# ingilizce arama yaparkenki kullanılan cümle yapısına yakındır.
# önce verileri yazdıralım.
# bunun için SQL Sorgusu şu şekilde
# SEÇ alanları NEREDEN tablo 
# SELECT id,ad,fiyat,tarih FROM cuzdan

import sqlite3
conn = sqlite3.connect('cuzdan.db')

c = conn.cursor()

# buradaki yazdığımız cüzdan tablo adı karışmasın.
# liste = c.execute("SELECT * from cuzdan ")
# 
# # for i in liste :
# #     print(i)
# 
# print(c.fetchone())

# ikinci aşamada veri girmek var.
# bu arada db browser programı açık olup yazma bekliyorsa
# aynı dosyayı iki program yazamayacağı için bekler.
# tek taraftan komut gönderip çalıştırın.

# EKLEME Komutu
# EKLE içine tablonun (alanlar..) DEĞERLER (değerler);
# sql komutları sonuna ; noktalı virgül koyabilirisniz.
# INSERT INTO cuzdan (ad,fiyat,tarih) VALUES("Silgi",11,"2011-10-05 12:55");

# c.execute('INSERT INTO cuzdan (ad,fiyat,tarih) VALUES("Defter",15,"2011-10-06 12:55");')
# 
# print(c.rowcount, "veri eklendi.")


# GÜNCELLEME komutu
# GÜNCELLE tabloyu ATAMAYAP alan=değer, alan=değer ...
# UPDATE cuzdan SET ad="Kalemlik" WHERE id = 1;

# c.execute('UPDATE cuzdan SET ad="Tükenmez Kalem" WHERE id=1')
# print(c.rowcount, "veri güncellendi.")


# SON olarak silme işlemi
# SİL NEREDEN tablo ÖYLEKİ ..
# DELETE FROM cuzdan WHERE id=2;


# c.execute('DELETE FROM cuzdan WHERE id=2')
# print(c.rowcount, "veri silindi.")




# Save (commit) the changes
# conn.commit()

liste = c.execute("SELECT * from cuzdan WHERE tarih<?",("2012-05-10",))
 
for i in liste :
    print(i)




















