'''
Created on Apr 9, 2019

@author: cam
'''
# Amacımız Veri Saklama ve Kullanma
# Fikir olarak cüzdan uygulaması yapalım
# Harcama Kalemi/Ürün , Fiyat ve Tarih bilgileri bulunsun.

# Bu bilgiyi liste halinde veya sözlük tipinde tutabiliriz.
# Her ikisine göre yazacağımız kodlarda bazı değişiklikler
# gerekebilir.
# liste tipi yapmayı mantıklı buluyorum ilk etapta.
# aslında sözlük tipi değişken aynı zamanda JSON formatına da
# uyuyor. Döküman tabanlı veritabanlarına da. Fakat bunu denemek
# size kalsın. Biz liste türünde yapacağız.
# örnek bir değişken :

# cuzdan = [
#         ["Kalem",5.0,"05-12-20019 12:15:00"],
#         ["Silgi",3.0,"06-12-20019 11:15:00"],
#     ]

# verileri bu formatta tutmayı planlıyorum.
# fakat tarih kısmı gördüğünüz üzere yazı tipinde.
# eğer böyle olursa tarihleri karşılaştırma ve 
# süzme yapamam. Bu nedenle tarih tipinde saklamam gerekli.
import datetime
import pickle 

cuzdan = []

# ürünü gösterecek => cuzdan[0][0]
# fiyatı gösterecek => cuzdan[x][1]
# tarihi gösterecek => cuzdan[x][2]

def dosyayaz():
    global cuzdan
    with open("veriler.pvd","wb") as f :
        pickle.dump(cuzdan,f)
    print("Dosya yazıldı.")

def dosyaoku():
    global cuzdan
    with open("veriler.pvd","rb") as f :
        cuzdan = pickle.load(f)
    print("Dosya okundu.")
# şimdi kullanıcıya bu bilgileri gösterelim.
# bir menü tasarlayalım.
menu = '''
Çıkmak için : 0
Tüm ürünleri görmek için : 1
Yeni ürün girmek için : 2
Bir ürün silmek için : 3
Ürün düzeltmek için : 4
Belli bir tarih aralığındaki harcama için :5 
Verileri yeniden yüklemek için : 6
Dosyayı kaydetmek için : 7
Tüm harcama toplamı için : 8
Belli bir ürünü adıyla aramak için: 9
'''
# başlangıçta verileri okuyup cuzdan değişkenine atayalım.
dosyaoku()
while (1 or True ) :
    girdi = input(menu)

    if(girdi == "0") :
        print("güle güle")
        dosyayaz()
        # çıkarken de otomatik kaydetsin...
        break
    elif(girdi == "1"): 
        # gösterimi daha güzel yapalım.
        print("{:<12}{:<12}{:<16}".format("Harcama","Fiyat", "Tarih"))
        for i in cuzdan : 
            print("{:<12}{:<12}{:<16}".format(i[0] ,i[1] ,i[2].strftime("%d/%m/%Y %H:%M")))

    elif(girdi == "2") :
        x = input("Ürün giriniz.")
        # tabi fiyat girdisi metin olduğu için float dönüşümü gerekli.
        y = float(input("Fiyat giriniz."))
        z = input("Tarih:gun/ay/yıl Saat:Dakika")
        # burada da veriyi datetime formatında girmek için
        z = datetime.datetime.strptime(z,"%d/%m/%Y %H:%M")
        cuzdan.append([x,y,z])
        pass
    elif(girdi == "3") :
        a = input("Silinecek ürünü yazınız.")
        for i in range(len(cuzdan)) :
            # range fonksiyonu nedeniyle rakamlar dönüyor
            # ilgili bilgi artık a indisinde.
            if(cuzdan[i][0] == a): 
                print(i+1, " anahtar numaralı bilgi",cuzdan[i][0])
        x = input ("Silinecek anahtar numarasını giriniz.")
        if ( int(x) < 0 and int(x)>len(cuzdan)-1):
            pass
        else :
            del cuzdan[int(x)-1]
        pass
    elif(girdi == "4") :
        # bakın buradaki kodlar tekrarlı duruyor
        # o yüzden burası için fonksiyon yazılsa iyi olurmuş...
        a = input("Düzeltilecek ürünü yazınız.")
        for i in range(len(cuzdan)) :
            # range fonksiyonu nedeniyle rakamlar dönüyor
            # ilgili bilgi artık a indisinde.
            if(cuzdan[i][0] == a): 
                print(i+1, " anahtar numaralı bilgi",cuzdan[i][0])
        x = input ("Düzeltilecek anahtar numarasını giriniz.")
        if ( int(x) < 0 and int(x)>len(cuzdan)-1):
            pass
        else :
            xu = input("Ürün giriniz.")
            # tabi fiyat girdisi metin olduğu için float dönüşümü gerekli.
            y = float(input("Fiyat giriniz."))
            z = input("Tarih:gun/ay/yıl Saat:Dakika")
            # burada da veriyi datetime formatında girmek için
            z = datetime.datetime.strptime(z,"%d/%m/%Y %H:%M")
            cuzdan[int(x)][0] = xu
            cuzdan[int(x)][1] = y
            cuzdan[int(x)][2] = z
            
        pass
    elif(girdi == "5") :
        # veri tabanı işlemlerinde en gıcık konu tarih konularıdır.
        # fakat biz zaten verimizi tarih formatında saklamıştık
        # işimiz biraz kolay.
        z = input("Başlangıç:gun/ay/yıl Saat:Dakika")
        z = datetime.datetime.strptime(z,"%d/%m/%Y %H:%M")
        z1 = input("Bitiş:gun/ay/yıl Saat:Dakika")
        z1 = datetime.datetime.strptime(z1,"%d/%m/%Y %H:%M")
        
        for i in cuzdan : 
            if(i[2]>=z and i[2]<=z1) :
                print(i)
        
        pass
    elif(girdi == "6") :
        dosyaoku()
        pass
    elif(girdi == "7") :
        dosyayaz()
        pass
    elif(girdi == "8") :
        print( sum([x[1] for x in cuzdan]) , " tl harcama yapıldı."  )
        pass
    elif(girdi == "9") :
        # burada bir parçasını vs de aramak mümkün
        # fakat o kısım size kalacak.
        # ben tam ürün adıyla aramasını yapacağım.
        a = input("Aranacak ürünü yazınız.")
        for i in cuzdan :
            if(a == i[0]):
                print("{:<12}{:<12}{:<16}".format(i[0] ,i[1] ,i[2].strftime("%d/%m/%Y %H:%M")))
        pass
    else :
        print("Yanlış bilgi girdiniz. Lütfen tekrar deneyiniz.")
    #print(girdi, "basıldı")
    print("=========================")









